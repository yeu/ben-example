// stdlib functionality
#include <iostream>
#include <fstream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

//#include "AthenaKernel/errorcheck.h"

int main() {
  int eventcount=0;
  int jetcount=0;
  
  // read config file for input parameters
  std::ifstream config("/home/atlas/Bootcamp/build-friend/config.txt");
  std::string fill;
  std::string val;
  std::string input_file_path;
  float pt_cut;
  float eta_cut;
  if ( config.is_open() ) {
    while( !config.eof() ) {
      config >> fill >> val;
      if ( fill=="input_file_path" ) {
	input_file_path=val;
      }
      else if ( fill=="pt_cut_GeV" ) {
	pt_cut=std::stof(val);
      }
      else if ( fill=="eta_cut" ) {
	eta_cut=std::stof(val);
      }
      else {
	std::cout<<"Error: Input not a parameter!"<<std::endl;
      }
    }
    config.close();
  }

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = input_file_path;
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );


  // make histograms for storage
  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);

  TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",100,0,500);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {
    eventcount=eventcount+1;

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if (!event.retrieve (ei, "EventInfo").isSuccess()) {
      std::cout<<"poop"<<std::endl;
    }
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");
    
    jetcount=jetcount+1;
    // make temporary vector of jets for those which pass selection
    std::vector<xAOD::Jet> jets_raw;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // make kinematic cuts for jets
      if( (jet->pt()/1000.)<pt_cut ){
	continue;
      }

      if( std::abs(jet->eta())>=eta_cut ){
	continue;
      }

      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      jets_raw.push_back(*jet);

    }

    // fill the analysis histograms accordingly
    h_njets_raw->Fill( jets_raw.size() );

    if( jets_raw.size()>=2 ){
      h_mjj_raw->Fill( (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000. );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // open TFile to store the analysis histogram output
  TFile *fout = new TFile("myOutputFile.root","RECREATE");

  h_njets_raw->Write();

  h_mjj_raw->Write();

  fout->Close();

  std::cout<<"Event Count: "<<eventcount<<"	"<<"Jet Count: "<<jetcount<<std::endl;

  // exit from the main function cleanly
  return 0;
}
